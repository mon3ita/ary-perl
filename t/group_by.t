use strict;
use warnings;

use Test::Simple tests => 1;

use Ary qw( group_by );

sub mod {
    my $val = shift;

    return $val % 5;
}

my @arr = (1..20);

my %result = (
    0 => [5, 10, 15, 20],
    1 => [4, 6, 11, 16],
    2 => [3, 7, 12, 17],
    3 => [2, 8, 13, 18],
    4 => [1, 9, 14, 19],
);

my $routine = \&mod;
ok( Ary::group_by($routine, \@arr) == %result);