use strict;
use warnings;

use Test::Simple tests => 2;

use Ary qw( in );

my @collection = (1..10);
my $val = 5;

ok(Ary::in(\@collection, $val));
ok(~ Ary::in(\@collection, 20));