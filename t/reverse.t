use strict;
use warnings;

use Test::Simple tests => 1;

use Ary qw( reverse );

my @collection = (1..5);
my $expected = join(" ", (5, 4, 3, 2, 1));

my $found = join(" ", Ary::reverse(\@collection));

ok($expected eq $found);