#!/usr/bin/perl

package Ary;

use Exporter;

our @EXPORT = qw( filter group_by in reverse );

sub filter {
    my $by = shift;
    my $collection = \@{$_[0]};
    my @result = ();

    foreach $value (@$collection) {
        if(&$by($value)) {
            push(@result, $value);
        }
    }

    return @result;
}

sub group_by {
    my $by = shift;
    my $collection = \@{$_[0]};
    my %result = ();

    foreach $value (@$collection) {
        my $current = &$by($value);

        if (~ exists($result{$current})) {
            @{$result{$current}} = ();
        }

        my $arr = \@{$result{$current}};
        push(@$arr, $value);
    }

    return %result;
}

sub in {
    my $collection = shift;
    my $value = shift;

    my $size = @$collection;
    for($i = 0, $j = $size - 1; $i < $size / 2; $i++, $j--) {
        if($$collection[$i] == $value or $$collection[$j] == $value) {
            return 1;
        }
    }

    return 0;
}

sub reverse {
    my $collection = shift;

    my @result = ();

    $size = @$collection;
    for($i = 0, $j = $size - 1; $i < $size / 2; $i++, $j--) {
        $result[$i] = $$collection[$j];
        $result[$j] = $$collection[$i];
    }

    return @result;
}

1;